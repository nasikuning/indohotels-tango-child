=== Theme Name ===
Contributors: Amri Karisma
Donate link: https://www.amrikarisma.com/
Tags: simple, seo
Requires at least: 4.8
Tested up to: 4.9.1
Version: 2.0.4
Stable tag: v2.0.4
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

== Description ==
This is the long description.  No limit, and you can use Markdown (as well as in the following sections).


== Installation ==
This is the long description.  No limit, and you can use Markdown (as well as in the following sections).

== Screenshots ==

1. This screen shot description corresponds to screenshot-1.(png|jpg|jpeg|gif). Note that the screenshot is taken from
the /assets directory or the directory that contains the stable readme.txt (tags or trunk). Screenshots in the /assets
directory take precedence. For example, `/assets/screenshot-1.png` would win over `/tags/4.3/screenshot-1.png`
(or jpg, jpeg, gif).
2. This is the second screen shot

== Frequently Asked Questions ==
This is the long description.  No limit, and you can use Markdown (as well as in the following sections).

== Changelog ==

= 2.0.4 =
* adjust pagination in room template

= 2.0.3 =
* Make Parrent menu navbar clickable

= 2.0.2 =
* Make Parrent menu navbar clickable

= 2.0.1 =
* Make Parrent menu navbar clickable

= 2.0.0 =
* Make Parrent menu navbar clickable

== Upgrade Notice ==

= 1.0 =
Upgrade notices describe the reason a user should upgrade.  No more than 300 characters.

`<?php code(); // goes in backticks ?>`
