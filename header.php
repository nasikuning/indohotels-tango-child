<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<header class="header-image-page clear" <?php krs_header_cover(); ?> role="banner">
		<!-- nav -->
		<nav id="mainNav" class="navbar navbar-custom navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span><i class="fa fa-bars"></i>
					</button>
					<div class="box-logo">
						<a href="<?php echo get_home_url(); ?>" class="logo-def"><img src="<?php echo ot_get_option('krs_logo'); ?>" alt="" class="img-responsive"></a>
	          <a href="<?php echo get_home_url(); ?>" class="logo-alt"><img src="<?php echo ot_get_option('krs_logo2'); ?>" alt="" class="img-responsive"></a>
					<?php //krs_headlogo(); ?>
					</div>
				</div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<?php karisma_nav(); ?>
				</div>

				<a href="#" class="book_trigger clearfix">Book Now</a>
				<div class="book-trigger" style="display: none">
					<section class="booking-section">
						<div class="booking-box">
							<?php do_shortcode("[booking_engine]"); ?>
						</div>
					</section>
				</div>

			</div>
		</nav>
		<!-- /nav -->
	</header>
	<!-- /header -->
