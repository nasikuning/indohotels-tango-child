<?php get_header('image'); ?>

<main role="main" class="col-md-12">
<div class="container">
	<!-- section -->
	<section class="box-content">

		<?php if (have_posts()): while (have_posts()) : the_post(); ?>
			<!-- article -->
			<article id="post-<?php the_ID(); ?>" <?php post_class('row'); ?>>
				<div class="box-book-rooms"><!-- box booking details -->
					<div class="room-details">
						<!-- <div class="col-sm-12 col-md-12"> -->
							<div class="room-booking">
								<div class="room-box">
									<div class="room-title-box text-center">
										<h2 class="room-title"><?php the_title(); ?></h2>
									</div>
									<!-- Slider -->
									<div class="span12" id="slider">
										<!-- Top part of the slider -->
										<div class="span8" id="carousel-bounding-box">
											<div class="carousel slide" id="myCarousel">
												<!-- Carousel items -->
												<div class="carousel-inner">
													<?php
													$images = rwmb_meta( 'indohotels_imgadv', 'size=gallery-slide' );
													if ( !empty( $images ) ) {
														$i = 0;
														foreach ( $images as $image ) {
															if($i++ == 0) {
																$active = 'active';
															} else {$active = '';}
															echo '<div class="'. $active .' item">';
															echo '<img src="', esc_url( $image['url'] ), '"  alt="', esc_attr( $image['alt'] ), '">';
															echo '</div>';
														}
													}
													?>
												</div>
												<!-- Carousel nav -->
												<a class="carousel-control left" data-slide="prev" href="#myCarousel"><span><i class="fa fa-chevron-left" aria-hidden="true"></i></span></a>
												<a class="carousel-control right" data-slide="next" href="#myCarousel"><span><i class="fa fa-chevron-right" aria-hidden="true"></i></span></a>
											</div>
										</div>
									</div><!--/Slider-->
								</div><!-- end .room-box -->

								<div class="room-details-desc">
									<div class="row">
										<div class="col-md-12">
											<ul class="room-info">
											<?php if(!empty(rwmb_meta( 'room_size' ))) : ?>
											<li>
												<span class="room-value">Room Size</span><span> : </span>
												<span><?php echo rwmb_meta( 'room_size' ); ?> m<sup>2</sup></span>
											</li>
											<?php endif; ?>
											<?php if(!empty(rwmb_meta( 'room_view' ))) : ?>
											<li>
												<span><span class="room-value">View</span><span> : </span>
												<span><?php echo rwmb_meta( 'room_view' ); ?></span>
											</li>
											<?php endif; ?>
											<?php if(!empty(rwmb_meta( 'room_occupancy' ))) : ?>
											<li>
												<span><span class="room-value">Ocupancy </span><span> : </span>
												<span><?php echo rwmb_meta( 'room_occupancy' ); ?> Person</span>
											</li>
											<?php endif; ?>
											<?php if(!empty(rwmb_meta( 'bed_size' ))) : ?>
											<li>
												<span><span class="room-value">Bed Size</span><span> : </span>
												<span><?php echo rwmb_meta( 'bed_size' ); ?></span>
											</li>
											<?php endif; ?>
											</ul>
										</div>
										<div class="col-md-12">
											<?php the_content(); ?>
										</div>
									</div>
								</div>
						</div>

					</article>
					<!-- /article -->

				<?php endwhile; ?>

			<?php else: ?>

				<!-- article -->
				<article>

					<h1><?php _e( 'Sorry, nothing to display.', 'indohotels' ); ?></h1>

				</article>
				<!-- /article -->

			<?php endif; ?>

		</section>
		<!-- /section -->
		</div>
	</main>

	<?php get_footer(); ?>
