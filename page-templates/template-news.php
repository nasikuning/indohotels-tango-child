<?php
/* Template Name: News Template */ get_header('plain'); ?>

<main role="main">
	<div class="container"> <!-- container -->
		<!-- section -->
		<section class="inner-news">

				<?php
				$args = array(
					'post_type' => 'post',
					'category_name' => 'news',
				);
				query_posts($args);
				if (have_posts()): while (have_posts()) : the_post(); ?>
				<div class="box-container">

						<!-- article -->
						<article id="post-<?php the_ID(); ?>">
							<div class="row">
								<div class="col-md-4 col-sm-4 col-xs-12">
									<div class="thumb">
										<?php if ( has_post_thumbnail()) :  ?>
											<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
												<?php the_post_thumbnail(array(300,150)); ?>
											</a>
										<?php endif; ?>
									</div>
								</div>
								<div class="col-md-8 col-sm-8 col-xs-12">
									<div class="box-text">
										<h2 class="title-room-list"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
										<span class="news-meta">by <?php the_author(); ?>, at <?php the_date(); ?></span>
										<?php	echo wp_trim_words( get_the_content(), 40, '...' ); ?>
									</div>
								</div>
							</div>
						</article>

				</div>
				<?php endwhile; ?>
				<?php else: ?>
					<article>
						<h2><?php _e( 'Sorry, nothing to display.', karisma_text_domain ); ?></h2>
					</article>
				<?php endif; ?>
				<?php karisma_pagination(); ?>
		</section>
		<!-- /section -->
</div> <!-- end container -->
</main>

<?php get_footer(); ?>
