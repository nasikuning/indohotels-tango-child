<?php /* Template Name: Home Template */ get_header('home'); ?>
<main role="main" class="homepage">

	<?php
	$style= '';
	if(!empty(ot_get_option('krs_background_text1'))){
        $style = 'style="background-image: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('. ot_get_option('krs_background_text1').')";';
    } ?>
	<section id="welcome-text"  <?php echo $style; ?>>
		<div class="container">
			<div class="box-bg padtb-large">
				<h1><?php echo ot_get_option('krs_ros_in_title');?></h1>
				<?php echo ot_get_option('krs_ros_in');?>
			</div>
		</div>
	</section>

	<?php top_deals(); ?>

  <?php if (ot_get_option('krs_room_actived') != 'off') : ?>
	<section id="room-section" class="padtb-large">
    <div class="container">

      <?php
        $args = array(
          'post_type'=> ot_get_option('krs_section_2'),
          'posts_per_page' => 6,
      );
      //query_posts($args);
      $krs_query = new WP_Query( $args );

      $count = $krs_query->post_count;

      if(($count == 2) || ($count == 4)) {
          $col = 'col-md-6';
      } else {
          $col = 'col-md-4';
      }

      if ($krs_query->have_posts()): ?>
        <div class="box-bg" <?php if($col == 'col-md-4') { echo 'style="width: 100%";'; } ?>>
          <h2><?php echo ot_get_option('krs_headline_section2');?></h2>
          <div class="row">
            <?php while ($krs_query->have_posts()) : $krs_query->the_post(); ?>
            <div class="<?php echo $col ?>">
              <div class="box-room">
                <div class="thumb">
                  <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
                  <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                      <?php the_post_thumbnail('gallery-slide'); // Declare pixel size you need inside the array ?>
                  </a>
                  <?php endif; ?>
                </div>
                <h4><?php the_title(); ?></h4>
              </div><!-- end .box-room -->
            </div>
            <?php endwhile; ?>
          </div><!-- end .row -->
          <?php endif; ?>
        </div> <!-- /box-bg -->
    </div><!-- end .container -->
  </section>
  <!-- /section -->
 <?php endif; ?>

  <?php if (ot_get_option('krs_section3_actived') != 'off') : ?>
  <section class="hotel-property text-center">
    <div class="container">
      <?php
  		$args = array(
				'post_type'=> ot_get_option('krs_section_3'),
  			'category_name' => 'property'
  			);
  		query_posts($args);
  		if (have_posts()) : ?>
      <h3><?php echo ot_get_option('krs_headline_section3');?></h3>
      <span class="line"></span>
      <div class="row">
        <?php while (have_posts()) : the_post(); ?>
        <div class="item col-md-4">
          <div class="thumbnails">
            <?php if ( has_post_thumbnail() ) : ?>
            <a href="<?php the_post_thumbnail_url('gallery-slide'); ?>" title="<?php the_title_attribute(); ?>">
              <img class="image-popups" src="<?php the_post_thumbnail_url('gallery-slide'); ?>" />
              <div class="overlay"></div>
            </a>
            <?php endif; ?>
          </div>
          <h4><?php echo get_the_title(); ?></h4>
        </div>
        <?php endwhile; ?>
        <?php endif; ?>
        <div class="clearfix"></div>
      </div><!-- end .row -->
    </div><!-- end .container -->
  </section>
  <?php endif; ?>

  <?php if (ot_get_option('krs_section4_actived') != 'off') : ?>
  <section class="home-facility padtb-large">
    <div class="container">
      <div class="box-home-gallery">
        <h2>Our Facilities</h2>
        <div class="home-carousel owl-carousel home-gallery">
          <?php
          $args = array(
            'post_type' => 'hotel-info',
            'category_name'  => 'facilities',
            );
          query_posts($args);
          if (have_posts()) : while (have_posts()) : the_post(); ?>
          <div class="item thumb">
            <div class="thumbnails">
							<span>
              <!-- post thumbnail -->
              <?php if ( has_post_thumbnail()) : //Check if thumbnail exists ?>
                <?php the_post_thumbnail('gallery-slide'); // Declare pixel size you need inside the array ?>
              <?php endif; ?>
              <!-- /post thumbnail -->
							</span>
            </div><!-- end .thumbnails -->
            <!-- post title -->

            <div class="title-gallery-home">
              <h2><?php echo get_the_title(); ?></h2>
              <?php if (!empty(rwmb_meta('gallery_openning'))) : ?>
                <div class="gallery-time">Opening hours	: <span><?php echo rwmb_meta('gallery_openning'); ?></span> - <span><?php echo rwmb_meta('gallery_closing'); ?></span></div>
              <?php endif; ?>
              <?php if (!empty(rwmb_meta('gallery_telephone'))) : ?>
                <div class="gallery-telephone">
                  <div>Phone		: <span><?php echo rwmb_meta('gallery_telephone'); ?></span></div>
                </div>
              <?php endif; ?>
            </div>

          <!-- /post title -->
          </div>
          <?php endwhile; ?>
          <?php endif; ?>

        </div>
      </div><!-- end .box-home-gallery -->
    </div><!-- end .container -->
  </section>
  <?php endif; ?>

  <?php footer_slide(); ?>
</main>
<?php get_footer(); ?>
