<?php
/* Template Name: Facilities Template */ get_header('image'); ?>

<main role="main" class="col-md-12">
	<div class="container text-center">
		<!-- container -->
		<!-- section -->
		<section id="image-popups">
			<h1 class="title text-center"><?php the_title(); ?></h1>
			<?php
			$args = array(
				'post_type' => 'hotel-info',
				'category_name' => 'facilities',
			);
			query_posts($args);
			if (have_posts()): while (have_posts()) : the_post(); ?>
			<div class="box-container col-md-6">
				<div class="room-thumb thumbnail">
					<article id="post-<?php the_ID(); ?>" <?php post_class( 'rooms-post'); ?>>
						<div class="thumb">
							<span>
							<?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
								<a href="<?php the_post_thumbnail_url('gallery-slide'); ?>" title="<?php the_title_attribute(); ?>">
									<img class="image-popups" src="<?php the_post_thumbnail_url('gallery-slide'); ?>" />
								</a>
							<?php endif; ?>
							</span>
						</div>
						<div class="box-text text-center">
							<h4><?php the_title(); ?></h4>
								<div class="room-info">
									<?php echo rwmb_meta('indohotels_room_balcony'); ?>
								</div>
								<?php the_excerpt(); ?>
						</div>
					</article>
				</div>
			</div>

			<?php endwhile; ?>

			<?php else: ?>

			<!-- article -->
			<article>
				<h2>
					<?php _e( 'Sorry, nothing to display.', karisma_text_domain ); ?>
				</h2>
			</article>
			<!-- /article -->

			<?php endif; ?>

			<?php get_template_part('pagination'); ?>

		</section>
		<!-- /section -->
	</div>
	<!-- end container -->
</main>

<?php get_footer(); ?>
