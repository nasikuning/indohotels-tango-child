<?php
function my_theme_enqueue_styles() {

    $parent_style = 'karisma-style';

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles', 60 );

/* Coloring*/
function krs_main_color() {
    if ( function_exists( 'ot_get_option' ) ) {
        if ( ! empty(ot_get_option( 'krs_main_colorpicker') ) ) {
            $mainColor = ot_get_option( 'krs_main_colorpicker');
            $seColor = ot_get_option('krs_secound_colorpicker');
            $terColor = ot_get_option('krs_tertiary_colorpicker');
            if(!empty(ot_get_option('krs_main_fonts'))){
                $fontsColor = ot_get_option('krs_main_fonts');
            }

            $fontHover = ot_get_option('krs_font_hover');

            $navBg = ot_get_option('krs_nav_background');
            $navStyle = ot_get_option('krs_nav_style');

            $mainBg = ot_get_option('krs_main_background');
            $section3bg = ot_get_option('krs_section3_bg');

            $footerBg = ot_get_option('krs_footer_background');
            $footerStyle = ot_get_option('krs_footer_style');

            //Font body
            if(!empty(ot_get_option('krs_main_fonts'))){
                $font_body_long = str_replace('+',' ',$fontsColor['font-family']);
                if(strpos($font_body_long , ':')){
                        $font_body = substr($font_body_long, 0, strpos($font_body_long, ':')); // remove char after :
                } else {
                        $font_body = $font_body_long;
                }
            }
            require_once ( get_stylesheet_directory() . '/inc/custom-child-style.php' );
        }
    }
}
add_action( 'wp_head', 'krs_main_color' );

require_once ( get_stylesheet_directory() . '/inc/plugin-update-checker/plugin-update-checker.php' );
    $updateChecker = Puc_v4_Factory::buildUpdateChecker(
        'https://bitbucket.org/nasikuning/indohotels-tango-child',
        __FILE__,
        'indohotels-tango-child'
    );

    $updateChecker->setAuthentication( array(
        'consumer_key' => 'suuRULgmY5pFExcvjk',
        'consumer_secret' => 'HQZMzt3xHf2B8ZdfU7dvVtabYskZEaFJ',
    ) );

    $updateChecker->setBranch( 'master' );
?>
