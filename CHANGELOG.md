## 2.0.4 (01 Aug, 2018)
* adjust pagination in room template

## 2.0.3 (11 July, 2018)
* Adjust style

## 2.0.2 (11 July, 2018)
* Adjust style

## 2.0.1 (10 July, 2018)
* Adjust style

## 2.0.0 (2 July, 2018)
* Adjust sitewide
