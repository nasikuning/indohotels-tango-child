<?php get_header('image'); ?>

<main role="main" class="col-md-12">
<div class="container">
	<!-- section -->
	<section class="box-content">

		<?php if (have_posts()): while (have_posts()) : the_post(); ?>
			<!-- article -->
			<article id="post-<?php the_ID(); ?>" <?php post_class('row'); ?>>
				<div class="box-book-rooms"><!-- box booking details -->
					<div class="room-details">
						<!-- <div class="col-sm-12 col-md-12"> -->
							<div class="room-booking">
								<div class="room-box">
									<div class="room-title-box text-center">
										<h2 class="room-title"><?php the_title(); ?></h2>
									</div>
									<!-- Slider -->
									<div class="span12" id="slider">
										<!-- Top part of the slider -->
										<div class="span8" id="carousel-bounding-box">
											<div class="carousel slide" id="myCarousel">
												<!-- Carousel items -->
												<div class="carousel-inner">
													<?php
													$images = rwmb_meta( 'indohotels_imgadv', 'size=gallery-slide' );
													if ( !empty( $images ) ) {
														$i = 0;
														foreach ( $images as $image ) {
															if($i++ == 0) {
																$active = 'active';
															} else {$active = '';}
															echo '<div class="'. $active .' item">';
															echo '<img src="', esc_url( $image['url'] ), '"  alt="', esc_attr( $image['alt'] ), '">';
															echo '</div>';
														}
													}
													?>
												</div>
												<!-- Carousel nav -->
												<a class="carousel-control left" data-slide="prev" href="#myCarousel"><span><i class="fa fa-chevron-left" aria-hidden="true"></i></span></a>
												<a class="carousel-control right" data-slide="next" href="#myCarousel"><span><i class="fa fa-chevron-right" aria-hidden="true"></i></span></a>
											</div>
										</div>
									</div><!--/Slider-->
								</div><!-- end .room-box -->

								<div class="room-details-desc">
									<div class="row">
										<div class="col-md-12">
											<ul class="room-info">
											<?php if(!empty(rwmb_meta( 'room_size' ))) : ?>
											<li>
												<span class="room-value">Room Size</span><span> : </span>
												<span><?php echo rwmb_meta( 'room_size' ); ?> m<sup>2</sup></span>
											</li>
											<?php endif; ?>
											<?php if(!empty(rwmb_meta( 'room_view' ))) : ?>
											<li>
												<span><span class="room-value">View</span><span> : </span>
												<span><?php echo rwmb_meta( 'room_view' ); ?></span>
											</li>
											<?php endif; ?>
											<?php if(!empty(rwmb_meta( 'room_occupancy' ))) : ?>
											<li>
												<span><span class="room-value">Ocupancy </span><span> : </span>
												<span><?php echo rwmb_meta( 'room_occupancy' ); ?> Person</span>
											</li>
											<?php endif; ?>
											<?php if(!empty(rwmb_meta( 'bed_size' ))) : ?>
											<li>
												<span><span class="room-value">Bed Size</span><span> : </span>
												<span><?php echo rwmb_meta( 'bed_size' ); ?></span>
											</li>
											<?php endif; ?>
											</ul>
										</div>
										<div class="col-md-12">
											<?php the_content(); ?>
										</div>
									</div>
								</div>


							<?php /*
							<?php if(rwmb_meta( 'room_convenience' ) || rwmb_meta( 'room_indulgence' ) || rwmb_meta( 'room_comfort' )): ?>
							<h3 class="room-details-f-title">
								<?php _e('Room features', karisma_text_domain); ?>
							</h3>
							<div class="feature-box col-md-12">

								<?php if(!empty(rwmb_meta( 'room_convenience' ))): ?>
								<div class="col-md-4">
									<h4>
										<?php _e('For Your Convenience', karisma_text_domain); ?>
									</h4>
									<ul class="room-features">
										<?php
											$values = rwmb_meta( 'room_convenience' );
											foreach ( $values as $value )
											{
												echo '<li>'. $value . '</li>';
											}
											?>

									</ul>
								</div>
								<?php endif; ?>
								<?php if(!empty(rwmb_meta( 'room_indulgence' ))): ?>
								<div class="col-md-4">
									<h4>
										<?php _e('For Your Indulgence', karisma_text_domain); ?>
									</h4>

									<ul class="room-features">
										<?php
											$values = rwmb_meta( 'room_indulgence' );
											foreach ( $values as $value )
											{
												echo '<li>'. $value . '</li>';
											}
											?>
									</ul>
								</div>
								<?php endif; ?>
								<?php if(!empty(rwmb_meta( 'room_comfort' ))): ?>

								<div class="col-md-4">
									<h4>
										<?php _e('For Your Comfort', karisma_text_domain); ?>
									</h4>

									<ul class="room-features">
										<?php
											$values = rwmb_meta( 'room_comfort' );
											foreach ( $values as $value )
											{
												echo '<li>'. $value . '</li>';
											}
											?>
									</ul>
								</div>
								<?php endif; ?>

							</div>
							<?php endif; ?>
							*/ ?>

							<?php
							$data['propery_id'] = get_option('idn_booking_engine.propery_id');
							?>
							<div class="text-center">
								<a href="//www.indohotels.id/website/property/<?php echo $data['propery_id']; ?>" class="btn btn-check"><?php _e('Check Availability', karisma_text_domain); ?>
								</a>
							</div>
						</div>

					</article>
					<!-- /article -->

				<?php endwhile; ?>

			<?php else: ?>

				<!-- article -->
				<article>

					<h1><?php _e( 'Sorry, nothing to display.', 'indohotels' ); ?></h1>

				</article>
				<!-- /article -->

			<?php endif; ?>

		</section>
		<!-- /section -->
		</div>
	</main>

	<?php get_footer(); ?>
