jQuery(document).ready(function () {
    jQuery('.home-gallery').owlCarousel({
        loop: false,
        margin: 18,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        loop: true,
        nav: true,
        navText: ["<span class='glyphicon glyphicon-chevron-left'></span>", "<span class='glyphicon glyphicon-chevron-right'>"],
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                margin: 5,
            },
            600: {
                items: 3,
                margin: 8,

            },
            1024: {
                items: 4,
                margin: 14,
            }
        }
    });
    /* Testimonial */
    jQuery('.home-text-slide').owlCarousel({
        loop: false,
        margin: 18,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        loop: true,
        nav: true,
        navText: ["<span class='glyphicon glyphicon-chevron-left'></span>", "<span class='glyphicon glyphicon-chevron-right'>"],
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                margin: 5,
            },
            600: {
                items: 1,
                margin: 8,
            },
            1024: {
                items: 1,
                margin: 14,
            }
        }
    });
});