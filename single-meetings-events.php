<?php get_header('image'); ?>

<main role="main" class="col-md-12">
<div class="container">
	<!-- section -->
	<section class="box-contents">
		<h1 class="title text-center"><?php the_title(); ?></h1>
		<?php if (have_posts()): while (have_posts()) : the_post(); ?>
			<!-- article -->
			<article id="post-<?php the_ID(); ?>">
				<?php the_content(); ?>
			</article>
					<!-- /article -->

				<?php endwhile; ?>

			<?php else: ?>

				<!-- article -->
				<article>

					<h1><?php _e( 'Sorry, nothing to display.', 'indohotels' ); ?></h1>

				</article>
				<!-- /article -->

			<?php endif; ?>

		</section>
		<!-- /section -->
		</div>
	</main>

	<?php get_footer(); ?>
