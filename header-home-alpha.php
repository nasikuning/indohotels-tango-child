<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>
	<!-- header -->
	<header id="homeSliderCarousel" class="carousel slide header-home clear" role="banner">
			<!-- nav -->
		<nav id="mainNav" class="navbar navbar-custom navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span> <i class="fa fa-bars"></i>
					</button>
					<div class="box-logo">
						<a href="<?php echo get_home_url(); ?>" class="logo-def"><img src="<?php echo ot_get_option('krs_logo'); ?>" alt="" class="img-responsive"></a>
	          <a href="<?php echo get_home_url(); ?>" class="logo-alt"><img src="<?php echo ot_get_option('krs_logo2'); ?>" alt="" class="img-responsive"></a>
					<?php //krs_headlogo(); ?>
					</div>
				</div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<?php karisma_nav(); ?>
				</div>

				<a href="#" class="book_trigger clearfix">Book Now</a>
				<div class="book-trigger" style="display: none">
					<section class="booking-section">
						<div class="booking-box">
							<?php do_shortcode("[booking_engine]"); ?>
						</div>
					</section>
				</div>

			</div>
		</nav>
		<!-- /nav -->
		<!-- Wrapper for Slides -->

		<div class="carousel-inner">
			<?php
			if ( function_exists( 'ot_get_option' ) ) {
				$images = explode( ',', ot_get_option( 'krs_gallery', '' ) );
				if ( ! empty( $images ) ) {
					$i=0;
					foreach( $images as $id ) {
						if ( ! empty( $id ) ) {
							if($i++ == 0) {
								$active = 'active';
							} else {$active = '';}
							$full_img_src = wp_get_attachment_image_src( $id, 'gallery-slide-main' );
							$thumb_img_src = wp_get_attachment_image_src( $id, 'gallery-slide-main' );
							?>
							<div class="item <?php echo $active; ?>">
								<div class="home-slider" style="background-image:url('<?php echo $full_img_src[0]; ?>');"></div>
							</div>
							<?php
						}
					}
				}
			}
			?>
			<a class="left carousel-control" href="#homeSliderCarousel" data-slide="prev"><span class="icon-prev"></span></a>
			<a class="right carousel-control" href="#homeSliderCarousel" data-slide="next"><span class="icon-next"></span></a>
		</div>



	</header>
	<!-- /header -->
